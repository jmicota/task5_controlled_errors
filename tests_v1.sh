#!/usr/pkg/bin/bash

set -e

SANDBOX="/root/nowy" # ZMIEŃ MNIE

cd $SANDBOX

echo "--- CHMOD TESTS ---"

rm -rf *
touch f
touch g
chmod 644 g
chmod 775 f
touch dummy
chmod 777 dummy
rm dummy

ls -l f | grep -- '-rwxrwxr-x'  > /dev/null
ls -l g | grep -- '-rw-r--r--' > /dev/null

echo "777 f"
chmod 777 f
ls -l f | grep -- '-rwxrwxrwx' >/dev/null
ls -l g | grep -- '-rw-r--r--' >/dev/null


echo "777 f"
chmod 777 f
ls -l f | grep -- '-rwxrwxrwx' >/dev/null
ls -l g | grep -- '-rw-r--r--' >/dev/null


echo "777 f (should flip!)"
chmod 777 f
ls -l f | grep -- '-rwxrwxr-x' >/dev/null
ls -l g | grep -- '-rw-r--r--' >/dev/null


echo "777 f"
chmod 777 f
ls -l f | grep -- '-rwxrwxrwx' >/dev/null
ls -l g | grep -- '-rw-r--r--' >/dev/null


echo "777 g"
chmod 777 g
ls -l f | grep -- '-rwxrwxrwx' >/dev/null
ls -l g | grep -- '-rwxrwxrwx' >/dev/null


echo "777 g (should flip!)"
chmod 777 g
ls -l f | grep -- '-rwxrwxrwx' >/dev/null
ls -l g | grep -- '-rwxrwxr-x' >/dev/null

echo "Directories not affected?"

mkdir dir

echo "777 dir"
chmod 777 dir
stat dir | grep -- 'drwxrwxrwx' > /dev/null

echo "777 dir"
chmod 777 dir
stat dir | grep -- 'drwxrwxrwx' > /dev/null

echo "777 dir"
chmod 777 dir
stat dir | grep -- 'drwxrwxrwx' > /dev/null

echo "777 dir"
chmod 777 dir
stat dir | grep -- 'drwxrwxrwx' > /dev/null

echo "CHMOD TESTS OK"
echo
echo




echo "--- DEBUG TESTS ---"
rm -rf *
mkdir debug
mkdir folder
touch plik
ls | tr '\n' ' ' | grep "^debug folder plik $"  > /dev/null

echo "Removing plik"
rm plik
ls | tr '\n' ' ' | grep "^debug folder $" > /dev/null
ls debug | tr '\n' ' ' | grep "^plik $" > /dev/null

echo "Removing folder"
rm -r folder
ls | tr '\n' ' ' | grep "^debug $" > /dev/null
ls debug | tr '\n' ' ' | grep "^plik $" > /dev/null

echo "Removing debug/plik"
rm debug/plik
ls | tr '\n' ' ' | grep "^debug $" > /dev/null
ls debug | wc -l | grep "0"  > /dev/null

echo "Cleanup..."
rm -rf *

echo "What if debug is a file?"
echo "xD" > debug
touch plik
rm plik
ls | tr '\n' ' ' | grep "^debug $" > /dev/null
echo "Ok, plik was deleted permanently"
cat debug | grep "xD" > /dev/null
echo "Contents of debug are intact. Let's delete debug"
rm debug
ls | wc -l | grep "0"  > /dev/null

echo "DEBUG TESTS OK"
echo
echo




echo "--- FLIP TESTS ---"
rm -rf *

echo "1234567 > x (3 and 6 should flip)"
echo 1234567 > x
cat x | grep "1244577"  > /dev/null

echo "1234567 > x2 (3 and 6 should flip)"
echo 1234567 > x2
cat x2 | grep "^1244577$" > /dev/null

echo "1234567 > x (Now 1, 4 and 7 should flip)"
echo 1234567 >> x
cat x | tr '\n' ' ' | grep "^1244577 2235568 $" > /dev/null

echo "Printing stat for fun"
stat x
stat x2

echo "FLIP TESTS OK"
echo
echo